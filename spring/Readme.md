# SevDesk BankingApp

This folder contains a Spring Boot/Kotlin app for the backend sample exercise.

# Run interactively
JDK 11 needs to be installed.

```bash
./gradlew bootRun
```

# Build/run using Docker

```bash
./gradlew build
docker build --build-arg JAR_FILE=build/libs/sdbanking-spring-0.0.1-SNAPSHOT.jar -t hkolkhorst/sdbanking .
docker run -p 8080:8080 hkolkhorst/sdbanking
```