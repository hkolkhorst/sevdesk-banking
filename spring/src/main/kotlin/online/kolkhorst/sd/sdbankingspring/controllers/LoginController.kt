package online.kolkhorst.sd.sdbankingspring.controllers

import online.kolkhorst.sd.sdbankingspring.business.InvalidCredentialsException
import online.kolkhorst.sd.sdbankingspring.business.Permissions.validatedUserIdFromToken
import online.kolkhorst.sd.sdbankingspring.model.sto.LoginCredentials
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

object LoginConstants {
    const val AUTH_HEADER = "X-Authorization"
    const val DUMMY_TOKEN = "Open Sesame"
    var ADMIN_USER_ID = 0L
}

object LoginValidator {

    fun validatedUserIdFromRequest(request: HttpServletRequest, uriUserId: Long? = null): Long {
        val authtoken = request.getHeader(LoginConstants.AUTH_HEADER) ?: throw InvalidCredentialsException()
        val tokenUserId = validatedUserIdFromToken(authtoken)
        if (uriUserId != null && tokenUserId != uriUserId) {
            throw InvalidCredentialsException()
        }

        return tokenUserId
    }
}

@RestController
@RequestMapping("/authtokens")
class LoginController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createSession(@RequestBody credentials : LoginCredentials) : Map<String, Any>  {
        //FIXME if using a decentralized design, one option would be to
        // serialize user, expiration date; hash & sign this (e.g., using ECDSA) and verify on each node
        // for now, we just give everybody admin access...
        return mapOf("auth_token" to LoginConstants.DUMMY_TOKEN)
    }
}