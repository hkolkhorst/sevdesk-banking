package online.kolkhorst.sd.sdbankingspring.model

import com.fasterxml.jackson.annotation.*
import java.lang.UnsupportedOperationException
import java.time.OffsetDateTime
import javax.persistence.*
import kotlin.collections.ArrayList
import kotlin.math.roundToLong


@Entity
@Table(name = "account")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
sealed class BaseAccount(
        @Column(unique = true) var accountNumber: String,
        var name: String,
        //might be more appropriate to use javax Money in real application
        var balanceCents: Long,
        var currency: String,
        var pin: String,
        var overdraftLimitCents: Long,
        @JsonIgnore
        var lastDepositDate: OffsetDateTime,
        @JsonManagedReference("ownedReference")
        @ManyToOne//(fetch = FetchType.LAZY)
        var owner: User,
        @JsonManagedReference("administratedReference")
        @ManyToMany//(fetch = FetchType.LAZY)
        var administrators: MutableList<User>? = ArrayList(),
        @Id @GeneratedValue var id: Long? = null,
) {
        @Transient // for serialization of subclasses
        val type = this::class.java.simpleName
        //for easier access of value in base currency
        var balance : Double
                get() = (balanceCents / 100.0).roundToLong().toDouble()
                set(value) {balanceCents = (value * 100).roundToLong()}
        var overdraftLimit : Double
                get() = (overdraftLimitCents / 100.0).roundToLong().toDouble()
                set(value) {overdraftLimitCents = (value * 100).roundToLong()}
}

@Entity
class CheckingAccount(
        accountNumber: String,
        name: String,
        balanceCents: Long,
        currency: String,
        pin: String,
        overdraftLimitCents: Long,
        lastDepositDate: OffsetDateTime,
        owner: User,
        administrators: MutableList<User>? = ArrayList(),
        id: Long? = null
) : BaseAccount(accountNumber, name, balanceCents, currency, pin, overdraftLimitCents, lastDepositDate, owner, administrators, id)

@Entity
class FixedTermSavingAccount(
        accountNumber: String,
        name: String,
        balanceCents: Long,
        currency: String,
        pin: String,
        lastDepositDate: OffsetDateTime,
        owner: User,
        administrators: MutableList<User>? = ArrayList(),
        id: Long? = null
): BaseAccount(accountNumber, name, balanceCents, currency, pin, overdraftLimitCents = 0, lastDepositDate, owner, administrators, id) {
        override var overdraftLimitCents: Long
                get() = super.overdraftLimitCents
                set(value) {throw UnsupportedOperationException()}
}

//debatable whether credit cards should also be accounts
@Entity
class CreditCard(
        var cardNumber: String,
        var balanceCents: Long,
        var currency: String,
        var maxCreditCents: Long,
        @ManyToOne var backingAccount: CheckingAccount,
        @Id @GeneratedValue var id: Long? = null
)

@Entity
class Transaction(
        @ManyToOne var sourceAccount: BaseAccount,
        @ManyToOne var targetAccount: BaseAccount,
        var amountCents: Long,
        var currency: String,
        var date: OffsetDateTime,
        @Id @GeneratedValue var id: Long? = null
)