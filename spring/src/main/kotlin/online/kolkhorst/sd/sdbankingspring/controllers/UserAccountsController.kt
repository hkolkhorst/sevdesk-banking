package online.kolkhorst.sd.sdbankingspring.controllers

import online.kolkhorst.sd.sdbankingspring.business.Operation
import online.kolkhorst.sd.sdbankingspring.business.Permissions
import online.kolkhorst.sd.sdbankingspring.model.*
import online.kolkhorst.sd.sdbankingspring.model.sto.AccountRequestHolder
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/users/{userid}/accounts")
class UserAccountsController(
    private val userRepository: UserRepository,
    private val fixedTermSavingAccountRepository: FixedTermSavingAccountRepository,
    private val checkingAccountRepository: CheckingAccountRepository
) {

    @RequestMapping("/checking", method = [RequestMethod.GET])
    fun listCheckingAccounts(@PathVariable userid: Long, request: HttpServletRequest): List<CheckingAccount> {
        val userId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(userId, Operation.USER_ACCOUNTS_LIST)

        return checkingAccountRepository.findByOwner_Id(userid)
    }

    @RequestMapping("/checking", method = [RequestMethod.POST])
    @ResponseStatus(HttpStatus.CREATED)
    fun addCheckingAccount(
        @PathVariable userid: Long,
        @RequestBody requestedAccount: AccountRequestHolder,
        request: HttpServletRequest
    ): CheckingAccount {
        val authUserId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(authUserId, Operation.USER_ACCOUNTS_CREATE, userid)

        val owner = userRepository.findByIdOrNull(userid) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
        // could also add the owner to administrators; but assume that this is always implicitly true
        val adminIds = requestedAccount.administratorIds ?: listOf()
        val administrators = userRepository.findAllById(adminIds).requireNoNulls()

        val newCheckingAccount = requestedAccount.toCheckingAccount(owner, administrators.toMutableList())

        return checkingAccountRepository.save(newCheckingAccount)
    }

    @RequestMapping("/checking/{accountNumber}", method = [RequestMethod.GET])
    fun getCheckingAccount(
        @PathVariable userid: Long,
        @PathVariable accountNumber: String,
        request: HttpServletRequest
    ): CheckingAccount {
        val userId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(userId, Operation.USER_ACCOUNTS_LIST)

        return checkingAccountRepository.findByAccountNumber(accountNumber)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    @RequestMapping("/checking/{accountNumber}", method = [RequestMethod.PUT])
    fun updateCheckingAccount(
        @PathVariable userid: Long,
        @PathVariable accountNumber: String,
        request: HttpServletRequest
    ): CheckingAccount {
        TODO()
    }

    @RequestMapping("/saving", method = [RequestMethod.GET])
    fun listSavingsAccounts(@PathVariable userid: Long, request: HttpServletRequest): List<FixedTermSavingAccount> {
        val authUserId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(authUserId, Operation.USER_ACCOUNTS_LIST, userid)

        return fixedTermSavingAccountRepository.findByOwner_Id(userid)
    }

    @RequestMapping("/saving", method = [RequestMethod.POST])
    @ResponseStatus(HttpStatus.CREATED)
    fun addSavingsAccount(
        @PathVariable userid: Long,
        @RequestBody requestedAccount: AccountRequestHolder,
        request: HttpServletRequest
    ): FixedTermSavingAccount {
        val authUserId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(authUserId, Operation.USER_ACCOUNTS_CREATE, userid)

        val owner = userRepository.findByIdOrNull(userid) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
        val adminIds = requestedAccount.administratorIds ?: listOf()
        val administrators = userRepository.findAllById(adminIds).requireNoNulls()

        val newSavingAccount = requestedAccount.toSavingAccount(owner, administrators.toMutableList())

        return fixedTermSavingAccountRepository.save(newSavingAccount)
    }

    @RequestMapping("/saving/{accountNumber}", method = [RequestMethod.GET])
    fun getSavingAccount(
        @PathVariable userid: Long,
        @PathVariable accountNumber: String,
        request: HttpServletRequest
    ): FixedTermSavingAccount {
        val userId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(userId, Operation.USER_ACCOUNTS_LIST)

        return fixedTermSavingAccountRepository.findByAccountNumber(accountNumber) ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND
        )
    }

    @RequestMapping("/saving/{accountNumber}", method = [RequestMethod.PUT])
    fun updateSavingAccount(
        @PathVariable userid: Long,
        @PathVariable accountNumber: String,
        @RequestBody updatedAccount: AccountRequestHolder,
        request: HttpServletRequest
    ): FixedTermSavingAccount {
        TODO()
    }
}