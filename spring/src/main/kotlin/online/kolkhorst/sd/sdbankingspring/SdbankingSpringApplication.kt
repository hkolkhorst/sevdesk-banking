package online.kolkhorst.sd.sdbankingspring

import org.springframework.boot.autoconfigure.SpringBootApplication

import org.springframework.boot.runApplication

@SpringBootApplication
class SdbankingSpringApplication


fun main(args: Array<String>) {
    runApplication<SdbankingSpringApplication>(*args)
}

