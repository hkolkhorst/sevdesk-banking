package online.kolkhorst.sd.sdbankingspring.model.sto

import online.kolkhorst.sd.sdbankingspring.Util
import online.kolkhorst.sd.sdbankingspring.model.CheckingAccount
import online.kolkhorst.sd.sdbankingspring.model.FixedTermSavingAccount
import online.kolkhorst.sd.sdbankingspring.model.User
import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset

data class AccountRequestHolder(
    val name: String,
    val pin: String,
    val overdraftLimitCents: Long?,
    val ownerId: Long?,
    val administratorIds: MutableList<Long>? = ArrayList()
) {
    init {
        overdraftLimitCents?.let { l -> require(l >= 0) }
    }


    private fun validate_relations(
        owner: User,
        administrators: MutableList<User>?
    ) {
        ownerId?.let { oId -> require(owner.id == oId) }
        administrators?.let { admins ->
            val adminIds = administratorIds ?: listOf<Long>()
            require(admins.size == adminIds.size)
            require(setOf(admins.map { a -> a.id }).equals(setOf(adminIds)))
        }
    }


    fun toCheckingAccount(owner: User, administrators: MutableList<User>?): CheckingAccount {
        require(overdraftLimitCents != null)
        validate_relations(owner, administrators)
        // create deterministic account numbers
        // this implicitly enforces distinct names for each customer's accounts due to unique account number requirement
        val accountNumber = "C" + Util.sha256hash("${owner.id}||$name")
        return CheckingAccount(
            accountNumber = accountNumber,
            name = name,
            balanceCents = 0,
            currency = "EUR",
            pin = pin,
            overdraftLimitCents = overdraftLimitCents,
            owner = owner,
            administrators = administrators,
            lastDepositDate = OffsetDateTime.of(LocalDate.EPOCH, LocalTime.NOON, ZoneOffset.UTC)
        )
    }


    fun toSavingAccount(owner: User, administrators: MutableList<User>?): FixedTermSavingAccount {
        require(overdraftLimitCents == null)
        validate_relations(owner, administrators)
        // create deterministic account numbers
        // this implicitly enforces distinct names for each customer's accounts due to unique account number requirement
        val accountNumber = "T" + Util.sha256hash("${owner.id}||$name")
        return FixedTermSavingAccount(
            accountNumber = accountNumber,
            name = name,
            balanceCents = 0,
            currency = "EUR",
            pin = pin,
            owner = owner,
            administrators = administrators,
            lastDepositDate = OffsetDateTime.of(LocalDate.EPOCH, LocalTime.NOON, ZoneOffset.UTC)
        )
    }
}