package online.kolkhorst.sd.sdbankingspring.model.sto

data class TransferRequestHolder(val sourceAccountNumber: String, val targetAccountNumber: String, val transferAmountCents: Long, val pin: String) {
    init {
        require(transferAmountCents > 0)
    }
}