package online.kolkhorst.sd.sdbankingspring.controllers

import org.hibernate.exception.ConstraintViolationException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.ServletWebRequest
import java.io.IOException


@ControllerAdvice
class CustomErrorHandler {
    var logger: Logger = LoggerFactory.getLogger(CustomErrorHandler::class.java)

    //map hibernate constraint exception (e.g., trying to insert duplicates into unique col) to response code
    @ExceptionHandler(ConstraintViolationException::class)
    @Throws(IOException::class)
    fun handleConstraintViolationException(
        exception: ConstraintViolationException,
        webRequest: ServletWebRequest
    ) {
        logger.info("mapping database constraint validation to bad request [${exception.message}]")
        webRequest.response!!.sendError(HttpStatus.BAD_REQUEST.value(), "data constraints violated")
    }
}
