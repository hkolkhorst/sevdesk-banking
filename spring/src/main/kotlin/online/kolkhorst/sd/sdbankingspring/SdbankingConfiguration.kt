package online.kolkhorst.sd.sdbankingspring

import online.kolkhorst.sd.sdbankingspring.controllers.LoginConstants
import online.kolkhorst.sd.sdbankingspring.model.*
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDate
import java.time.OffsetDateTime

@Configuration
class SdbankingConfiguration {

    @Bean
    fun databaseInitializer(
        userRepository: UserRepository, checkingAccountRepository: CheckingAccountRepository,
        fixedTermSavingAccountRepository: FixedTermSavingAccountRepository
    ) = ApplicationRunner {
        //FIXME do this, e.g., based on environment variable indicating testing
        val tmpUser = User(
            lastname = "Admin", firstname = "Test",
            birthday = LocalDate.of(1969, 2, 1),
            gender = "nb",
            username = "admin", password = "OpenSesame"
        )
        val user = userRepository.save(tmpUser)
        LoginConstants.ADMIN_USER_ID = user.id!!

//        checkingAccountRepository.save(
//            CheckingAccount(
//                accountNumber = "123",
//                name = "TestAccount",
//                balanceCents = 0,
//                currency = "EUR",
//                pin = "4242",
//                overdraftLimitCents = 0,
//                owner = user,
//                lastDepositDate = OffsetDateTime.of(LocalDate.EPOCH, LocalTime.NOON, ZoneOffset.UTC)
//            )
//        )
//        fixedTermSavingAccountRepository.save(
//            FixedTermSavingAccount(
//                accountNumber = "1234",
//                name = "TestAccount",
//                balanceCents = 0,
//                currency = "EUR",
//                pin = "4242",
//                owner = user,
//                lastDepositDate = OffsetDateTime.of(LocalDate.EPOCH, LocalTime.NOON, ZoneOffset.UTC)
//            )
//        )
    }
}