package online.kolkhorst.sd.sdbankingspring.business

import online.kolkhorst.sd.sdbankingspring.controllers.LoginConstants
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

enum class Operation {
    USER_ADD, USER_MODIFY, USER_READ, USER_ACCOUNTS_LIST, USER_ACCOUNTS_CREATE, USER_ACCOUNTS_TRANSFER
}

@ResponseStatus(HttpStatus.FORBIDDEN)
class InvalidCredentialsException : RuntimeException() {}

object Permissions {
    var logger: Logger = LoggerFactory.getLogger(Permissions::class.java)

    fun isAuthorized(userId: Long, operation: Operation, operandId: Long? = null): Boolean {
        if (userId == LoginConstants.ADMIN_USER_ID) {
            logger.info("authorizing admin user to ${operation} on $operandId")
            return true
        }

        val isAuthorized = when(operation) {
            Operation.USER_ADD -> false  //admin only
            //for the following the operandId is the user id
            Operation.USER_READ -> userId == operandId
            Operation.USER_MODIFY -> userId == operandId
            Operation.USER_ACCOUNTS_LIST -> userId == operandId
            Operation.USER_ACCOUNTS_CREATE -> userId == operandId
            //for the following the operandId is the account id
            Operation.USER_ACCOUNTS_TRANSFER -> userId == operandId
        }
        val logDecision = if (isAuthorized) "authorizing" else "denying"
        logger.info("$logDecision user $userId to $operation on $operandId")

        return isAuthorized
    }
    fun assureAuthorization(userId: Long, operation: Operation, operandId: Long? = null) {
        if (!isAuthorized(userId, operation, operandId)) {
            throw SecurityException()
        }
    }

    fun validatedUserIdFromToken(authtoken: String) : Long {
        //FIXME: this could check signature and hash of the token and then return the user id
        // the following is just dummy code
        if (authtoken == LoginConstants.DUMMY_TOKEN) {
            return LoginConstants.ADMIN_USER_ID
        } else {
            throw InvalidCredentialsException()
        }
    }
}