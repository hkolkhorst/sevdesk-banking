package online.kolkhorst.sd.sdbankingspring.model.sto

data class LoginCredentials(val username: String, val password: String)