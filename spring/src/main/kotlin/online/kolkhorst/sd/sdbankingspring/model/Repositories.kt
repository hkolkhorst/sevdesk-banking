package online.kolkhorst.sd.sdbankingspring.model

import org.springframework.data.repository.CrudRepository


interface UserRepository: CrudRepository<User, Long> {
}

interface AccountRepository: CrudRepository<BaseAccount, Long> {
    fun findByOwner_Id(ownerId: Long): List<BaseAccount>

//    @Query("SELECT p FROM BaseAccount p where TYPE(p)=:discriminatorValue")
//    fun findByType(@Param("discriminatorValue") discriminatorValue: String?): List<BaseAccount>
}

interface CheckingAccountRepository: CrudRepository<CheckingAccount, Long> {
    fun findByAccountNumber(accountNumber: String): CheckingAccount?
    fun findByOwner_Id(ownerId: Long): List<CheckingAccount>
}

interface FixedTermSavingAccountRepository: CrudRepository<FixedTermSavingAccount, Long> {
    fun findByAccountNumber(accountNumber: String): FixedTermSavingAccount?
    fun findByOwner_Id(ownerId: Long): List<FixedTermSavingAccount>
}

interface TransactionRepository : CrudRepository<Transaction, Long> {

}