package online.kolkhorst.sd.sdbankingspring.controllers

import online.kolkhorst.sd.sdbankingspring.business.InvalidCredentialsException
import online.kolkhorst.sd.sdbankingspring.business.Operation
import online.kolkhorst.sd.sdbankingspring.business.Permissions
import online.kolkhorst.sd.sdbankingspring.model.*
import online.kolkhorst.sd.sdbankingspring.model.sto.TransferRequestHolder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.time.OffsetDateTime
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/transfer")
class TransferController(
    private val accountRepository: AccountRepository,
    private val checkingAccountRepository: CheckingAccountRepository,
    private val savingAccountRepository: FixedTermSavingAccountRepository,
    private val transactionRepository: TransactionRepository
) {
    var logger: Logger = LoggerFactory.getLogger(TransferController::class.java)

    @PostMapping
    @Transactional //rollback in case of problems
    fun performTransfer(@RequestBody transferRequest: TransferRequestHolder, request: HttpServletRequest) {
        val userId = LoginValidator.validatedUserIdFromRequest(request)
        executeTransferRequest(transferRequest, userId)
    }

    private fun executeTransferRequest(
        transferRequest: TransferRequestHolder,
        userId: Long
    ) {
        val sourceAccount = retrieveAccount(transferRequest.sourceAccountNumber)
        Permissions.assureAuthorization(userId, Operation.USER_ACCOUNTS_TRANSFER, sourceAccount.id)
        if (sourceAccount.pin != transferRequest.pin) {
            logger.info("invalid credentials [user=$userId,account=${sourceAccount.accountNumber}]")
            throw InvalidCredentialsException()
        }

        val targetAccount = retrieveAccount(transferRequest.targetAccountNumber)

        if (sourceAccount.currency != targetAccount.currency) {
            logger.info("incompatible currencies")
            throw ResponseStatusException(HttpStatus.CONFLICT)
        }

        //FIXME the following checks would need to be atomic on the database level, locked,
        // or rolled back if the source account has changed intermittently
        when (sourceAccount) {
            is CheckingAccount -> {
                if (sourceAccount.balanceCents + sourceAccount.overdraftLimitCents < transferRequest.transferAmountCents) {
                    logger.info("insufficient funds [user=$userId,account=${sourceAccount.accountNumber}]")
                    throw ResponseStatusException(HttpStatus.CONFLICT)
                }
            }
            is FixedTermSavingAccount -> {
                if (sourceAccount.balanceCents < transferRequest.transferAmountCents) {
                    logger.info("insufficient funds [user=$userId,account=${sourceAccount.accountNumber}]")
                    throw ResponseStatusException(HttpStatus.CONFLICT)
                }
                val earliestWithdrawalDate = sourceAccount.lastDepositDate.plusMonths(1)
                if (earliestWithdrawalDate.isAfter(OffsetDateTime.now())) {
                    logger.info("holding period not over [user=$userId,account=${sourceAccount.accountNumber},withdrawalDate=$earliestWithdrawalDate]")
                }
            }
        }

        sourceAccount.balanceCents -= transferRequest.transferAmountCents
        targetAccount.balanceCents += transferRequest.transferAmountCents
        targetAccount.lastDepositDate = OffsetDateTime.now()
        assert(sourceAccount.balanceCents >= -sourceAccount.overdraftLimitCents)
        val transaction = Transaction(
            sourceAccount = sourceAccount,
            targetAccount = targetAccount,
            amountCents = transferRequest.transferAmountCents,
            currency = sourceAccount.currency,
            date = OffsetDateTime.now()
        )

        accountRepository.save(sourceAccount)
        accountRepository.save(targetAccount)
        transactionRepository.save(transaction)
    }

    private fun retrieveAccount(accountNumber: String) =
        checkingAccountRepository.findByAccountNumber(accountNumber)
            ?: savingAccountRepository.findByAccountNumber(accountNumber)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
}