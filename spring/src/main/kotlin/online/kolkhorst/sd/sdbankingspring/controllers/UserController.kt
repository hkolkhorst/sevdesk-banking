package online.kolkhorst.sd.sdbankingspring.controllers

import online.kolkhorst.sd.sdbankingspring.business.Operation
import online.kolkhorst.sd.sdbankingspring.business.Permissions
import online.kolkhorst.sd.sdbankingspring.model.User
import online.kolkhorst.sd.sdbankingspring.model.UserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.lang.IllegalArgumentException
import javax.servlet.http.HttpServletRequest


@RestController
@RequestMapping("/users")
class UserController(private val userRepository: UserRepository) {


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createUser(@RequestBody req: User, request: HttpServletRequest): User {
        val curUserId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(curUserId, Operation.USER_ADD)
        //FIXME additional validation aside from valid datatypes
        //FIXME add location header (HATEOAS...)
        return userRepository.save(req)
    }

    @RequestMapping("/{userid}", method = arrayOf(RequestMethod.GET))
    fun readUser(@PathVariable userid: Long, request: HttpServletRequest): User {
        val curUserId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(curUserId, Operation.USER_READ, userid)
        val user = userRepository.findByIdOrNull(userid)
        return user ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    @RequestMapping("/{userid}", method = arrayOf(RequestMethod.PUT))
    fun updateUser(@RequestBody userUpdates: User, @PathVariable userid: Long, request: HttpServletRequest): User {
        val curUserId = LoginValidator.validatedUserIdFromRequest(request)
        Permissions.assureAuthorization(curUserId, Operation.USER_MODIFY, userid)
        userUpdates.id?.let { uUserId ->
            if (uUserId != userid) throw IllegalArgumentException()
        }
        userUpdates.id = userid
        userUpdates.password?.let { rawPw -> userUpdates.setPasswordHashFromRaw(rawPw) }
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        // FIXME additional validation or filling or handling of optional fields
        return userRepository.save(userUpdates)
    }
}