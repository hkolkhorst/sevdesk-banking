package online.kolkhorst.sd.sdbankingspring.model

import com.fasterxml.jackson.annotation.*
import online.kolkhorst.sd.sdbankingspring.Util.sha256hash
import java.security.MessageDigest
import java.time.LocalDate
import java.time.OffsetDateTime
import javax.persistence.*

//Note: we use the same class for (de)serialization and as a DTO;
//this might be inappropriate in practice but reduces boilerplate for this sample

@Entity
class User(
    var lastname: String,
    var firstname: String,
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    var birthday: LocalDate,
    var gender: String, //could also be enum (e.g., with m/f/nb)
    @Column(unique = true)
    var username: String,
    @JsonIgnore
    var password_sha256: String? = null, //omitting salt and hash algo column for simplicity
    @Transient @JsonIgnore
    var password: String? = null,
    @Id @GeneratedValue
    var id: Long? = null,
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    @JsonBackReference("ownedReference")
    var accountsOwned: MutableList<BaseAccount>? = ArrayList(),
    @ManyToMany(mappedBy = "administrators", fetch = FetchType.LAZY)
    @JsonBackReference("administratedReference")
    var accountsAdministrated: MutableList<BaseAccount>? = ArrayList()
) {
    fun setPasswordHashFromRaw(newPassword: String? = null): String {
        //this requires either the parameter or the field to be set
        val rawPassword = newPassword ?: password!!
        return sha256hash(rawPassword)
    }



}
