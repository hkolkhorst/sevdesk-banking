package online.kolkhorst.sd.sdbankingspring

import java.security.MessageDigest

object Util {
    fun sha256hash(input: String): String {
        val hashBytes = MessageDigest.getInstance("SHA-256")
            .digest(input.toByteArray())

        return hashBytes.fold("") { str, byteIt -> str + "%02x".format(byteIt) }
    }
}