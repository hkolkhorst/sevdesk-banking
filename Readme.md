# sevDesk Sample App

This repo contains a quick prototype version of the sevDesk banking
    sample app. I decided to give the Spring/Kotlin combo a spin/try
(``spring`` subfolder) and added some Gherkin-style integration tests
with a completely separate stack/implementation (``python``
subfolder).

Many design decisions have been made based on this being a quick
exercise rather than a product. These are partially indicated with
``FIXME`` comments in the code (we will talk about this later).

Some assumptions have been made with regard to ambiguities in the
   description. For example, for the requirement that money may only
   be withdrawn from the fixed-term savings account after a month: use
   the time since last deposit to the account rather than track time
   since each individual deposit separately for (partial) withdrawal.
