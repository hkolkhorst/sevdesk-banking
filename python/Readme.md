## Integration tests

Here we use BDD-style tests in python as integration tests for the Spring app.

Test scenarios can be found in ``sdtest/features``.

# Setup
Create a virtual env of your choice and run
```bash
pip install -e ./sdtest
```

# Run manually

```bash
pytest -v sdtest/test_banking.py
```