import dataclasses
import random

import requests
from pytest_bdd import scenarios, given, when, then

from .model.account import CheckingAccount, _assert_accounts_equal, SavingsAccount
from .model.transfer import Transfer
from .model.user import User, _assert_users_equal

scenarios('features/banking.feature')

# FIXME get host from environment variable/config file
HOST = 'http://localhost:8080'
AUTH_HEADER = 'X-Authorization'
LOGIN_RESSOURCE = '/authtokens'
USER_RESSOURCE = '/users'
TRANSFER_RESSOURCE = '/transfer'

@given("a user named <first> <last> with birthday <bday> and gender <gender>", target_fixture='user')
def build_user(first, last, bday, gender):
    return User(first, last, bday, gender, username=f"{first}_{last}", password="dummy")

@given("an authentication token", target_fixture='auth_token')
def obtain_auth_token():
    r = requests.post(f'{HOST}{LOGIN_RESSOURCE}', json={'username': 'foo', 'password': 'bar'})
    print(r.request)
    print(r.headers)
    assert r.status_code == 201
    return r.json()['auth_token']

@when("I create the user", target_fixture='created_user')
def create_user(user, auth_token):
    r = requests.post(f'{HOST}{USER_RESSOURCE}', json=dataclasses.asdict(user), headers={AUTH_HEADER: auth_token})

    print(r.request.headers)
    print(r.request.body)
    print('---')
    print(r.headers)
    assert r.status_code == 201
    print(r.json())
    print(dataclasses.fields(User))
    # do not deserialize fields we did not model
    params = {k: v for k, v in r.json().items() if k in [f.name for f in dataclasses.fields(User)]}
    print(params)
    return User(**params)


@then("the desired user should be created")
def check_user(user: User, created_user: User):
    print(user, created_user)
    _assert_users_equal(created_user, user)



@then("the user id should be set")
def create_user(created_user: User):
    assert created_user.id is not None


@then("the user should be readable at the URI")
def get_user(created_user, auth_token):
    # FIXME this should ideally be based on the Location header of the 201 response and not manually assembled
    user_uri = f'{HOST}{USER_RESSOURCE}/{created_user.id}'

    r = requests.get(user_uri, headers={AUTH_HEADER: auth_token})
    assert r.status_code == 200
    assert r.headers['Content-Type'] == 'application/json'

    params = {k: v for k, v in r.json().items() if k in [f.name for f in dataclasses.fields(User)]}
    print(params)
    retrieved_user = User(**params)
    _assert_users_equal(retrieved_user, created_user)


@when("I choose a random PIN", target_fixture="pin")
def create_pin():
    return f"{random.randrange(1000):04d}"

def _dataclass_from_json(json_dict, cls):
    # do not deserialize fields we did not model
    params = {k: v for k, v in json_dict.items() if k in [f.name for f in dataclasses.fields(cls)]}
    return cls(**params)

@when("I create a checking account with <overdraft> EUR overdraft", target_fixture="checking_account")
def create_checking_account(overdraft, created_user, pin, auth_token):
    account_request = CheckingAccount("TestChecking", pin, ownerId=created_user.id,
                                      overdraftLimitCents=int(float(overdraft)*100))
    checking_uri = f'{HOST}{USER_RESSOURCE}/{created_user.id}/accounts/checking'

    r = requests.post(checking_uri, json=dataclasses.asdict(account_request), headers={AUTH_HEADER: auth_token})

    assert r.status_code == 201
    account_created = _dataclass_from_json(r.json(), CheckingAccount)
    _assert_accounts_equal(account_created, account_request)
    return account_created


@when("I create a fixed term savings account", target_fixture='savings_account')
def create_saving_account(created_user, pin, auth_token):
    account_request = SavingsAccount("TestSaving", pin)  # we purposefully don't set owner id
    savings_uri = f'{HOST}{USER_RESSOURCE}/{created_user.id}/accounts/saving'

    r = requests.post(savings_uri, json=dataclasses.asdict(account_request), headers={AUTH_HEADER: auth_token})

    assert r.status_code == 201

    account_created = _dataclass_from_json(r.json(), SavingsAccount)
    _assert_accounts_equal(account_created, account_request)
    return account_created


@when("I transfer <amount> EUR from account checking to saving")
def perform_transfer(amount, checking_account: CheckingAccount, savings_account : SavingsAccount, auth_token):
    transfer = Transfer(sourceAccountNumber=checking_account.accountNumber,
                        targetAccountNumber=savings_account.accountNumber,
                        transferAmountCents=int(float(amount) * 100), pin=checking_account.pin)
    transfer_uri = f'{HOST}{TRANSFER_RESSOURCE}'

    r = requests.post(transfer_uri, json=dataclasses.asdict(transfer), headers={AUTH_HEADER: auth_token})

    assert r.status_code == 200


@then("the savings account has a positive balance", target_fixture='dummy1')
def check_savings( created_user, savings_account, auth_token):
    checking_account_uri = f'{HOST}{USER_RESSOURCE}/{created_user.id}/accounts/saving/{savings_account.accountNumber}'
    r = requests.get(checking_account_uri, headers={AUTH_HEADER: auth_token})
    assert r.status_code == 200

    retrieved_account = _dataclass_from_json(r.json(), CheckingAccount)
    assert retrieved_account.accountNumber == savings_account.accountNumber
    assert retrieved_account.balanceCents > 0



@then("the checking account has a negative balance")
def check_checking( created_user, checking_account, auth_token):
    checking_account_uri = f'{HOST}{USER_RESSOURCE}/{created_user.id}/accounts/checking/{checking_account.accountNumber}'
    r = requests.get(checking_account_uri, headers={AUTH_HEADER: auth_token})
    assert r.status_code == 200

    retrieved_account = _dataclass_from_json(r.json(), CheckingAccount)
    assert retrieved_account.accountNumber == checking_account.accountNumber
    assert retrieved_account.balanceCents < 0
