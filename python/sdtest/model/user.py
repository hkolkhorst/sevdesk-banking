import datetime
from dataclasses import dataclass
from typing import Optional


@dataclass
class User:
    firstname: str
    lastname: str
    birthday: str  # we purposefully don't use a date to be able to send invalid dates to the API
    gender: str
    username: str
    password: Optional[str] = None
    id: Optional[int] = None


def _assert_users_equal(user_actual, user_expected):
    assert user_actual.lastname == user_expected.lastname
    assert user_actual.firstname == user_expected.firstname
    assert user_actual.gender == user_expected.gender
    assert datetime.datetime.fromisoformat(user_actual.birthday) == datetime.datetime.fromisoformat(user_expected.birthday)