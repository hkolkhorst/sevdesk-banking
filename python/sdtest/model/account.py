import datetime
from dataclasses import dataclass
from typing import Optional


@dataclass
class BaseAccount:
    name: str
    pin: str
    ownerId: Optional[int] = None
    administratorIds: Optional[list[int]] = None
    id: Optional[int] = None
    balanceCents: Optional[int] = None
    accountNumber: Optional[str] = None


@dataclass
class SavingsAccount(BaseAccount):
    pass


@dataclass
class CheckingAccount(BaseAccount):
    overdraftLimitCents: Optional[int] = None


def _assert_accounts_equal(account_actual : BaseAccount, account_expected : BaseAccount):
    assert account_actual.name == account_expected.name
    assert account_actual.pin == account_expected.pin
    if account_expected.id is not None:
        assert account_actual.id == account_expected.id
    if account_expected.balanceCents is not None:
        assert account_actual.balanceCents == account_expected.balanceCents
    if isinstance(account_expected, CheckingAccount):
        assert account_actual.overdraftLimitCents == account_expected.overdraftLimitCents
