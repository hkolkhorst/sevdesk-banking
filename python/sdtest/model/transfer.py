from dataclasses import dataclass


@dataclass
class Transfer:
    sourceAccountNumber: str
    targetAccountNumber: str
    transferAmountCents: int
    pin: str