Feature: Money transfer
  It is possible to create users and account as well as transfer money through the API.

  Scenario Outline: Transfer between own checking and saving account.
    Given a user named <first> <last> with birthday <bday> and gender <gender>
    And an authentication token

    When I create the user
    And I choose a random PIN
    And I create a checking account with <overdraft> EUR overdraft
    And I create a fixed term savings account
    And I transfer <amount> EUR from account checking to saving

    Then the desired user should be created
    And the user id should be set
    And the user should be readable at the URI
    And the savings account has a positive balance
    And the checking account has a negative balance

    Examples:
    | first    | last       |       bday | gender | overdraft | amount |
    | Michaela | Mustermann | 1969-01-01 | f      |        20 |     10 |
    | unic☼de  | gapyear    | 2020-02-29 | nb     |         1 |   0.01 |